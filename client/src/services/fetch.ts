const data: any = JSON.stringify({
	query: `{
        categories: productLists(ids: "156126", locale: de_DE) {
          name
          articleCount
          childrenCategories: childrenProductLists {
            list {
              name
              urlPath
            }
          }
          categoryArticles: articlesList(first: 50) {
            articles {
              name
              variantName
              prices {
                currency
                regular {
                  value
                }
              }
              images(
                format: WEBP
                maxWidth: 200
                maxHeight: 200
                limit: 1
              ) {
                path
              }
            }
          }
        }
      }`,
});

const proxy: any = {
	method: 'POST',
	headers: { 'Content-Type': 'application/json' },
	body: data,
};

export default class Services {
	async POST(url: string) {
		return await fetch(url, proxy)
			.then((res: any) => res.json())
			.then(({ data }: any) => data)
			.catch((error: any) => {
				console.log('Error :::', error);
				return;
			});
	}
}

export const fetchAPI: any = new Services();
