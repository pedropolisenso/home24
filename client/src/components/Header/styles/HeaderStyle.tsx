import styled from '@emotion/styled'
import { theme } from '../../../theme';

const HeaderStyle = styled.div`
	grid-area: header;
	background-color: ${theme.header.background};
	box-shadow: inset 0 -1px 0 ${theme.header.boxShadow.color};
	padding: 23px 0 4px 15px;
    position: fixed;
    width: 100%;
    background: white;
    z-index: 2;

	img {
		width: 118px;
	}

	input {
		float: right;
		height: 30px;
		width: 160px;
		padding-left: 5px;
		border: 1px solid ${theme.header.boxShadow.color};
		border-top-left-radius: 3px;
		border-bottom-left-radius: 3px;
		outline: none;
		order: 2;
	}

	button {
		order: 1;
		float: right;
		height: 34px;
		width: 80px;
		background-color: ${theme.primary};
		color: ${theme.header.search.button.color};
		border: 0;
		border-top-right-radius: 3px;
		border-bottom-right-radius: 3px;
		outline: none;
		margin-right: 25px;
		&:hover {
			cursor: pointer;
			opacity: 0.9;
		}
	}
`

export default HeaderStyle;
