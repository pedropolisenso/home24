import React from 'react';
import HeaderStyle from './styles/HeaderStyle';

const Header = ({ title }: { title: string }) => (
	<HeaderStyle>
		<img src='https://cdn1.home24.net/corgi/static/media/home-24-logo.4f73bd13.svg' alt={title} />
		<button type="submit" role="button">Search</button>
		<input type='text' placeholder='Produkte finden' alt='search product'/>
	</HeaderStyle>
);

export default Header;
