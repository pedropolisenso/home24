import styled from '@emotion/styled'
import { theme } from '../../../theme';

export const FooterStyle = styled.div`
    grid-area: footer;
    background-color: ${theme.footer.background};
    color: ${theme.footer.color};
    text-align: center;
    padding: 20px;
    font-size: 14px;
    margin-top: 10px;
`

export const TopStyle = styled.div`
    text-align: right;
    display: block;

    img {
        cursor: pointer;
        &:hover {
            opacity: 0.8;
        }
    }
`;
