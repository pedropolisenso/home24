import React from 'react';

import { FooterStyle, TopStyle } from './styles/FooterStyles';

const Footer = ({ text }: { text: string }) => (
	<div>
		<TopStyle onClick={() => window.scrollTo(0, 0)}>
			<img style={{ marginRight: 10 }} width="30" src='/assets/top.png' alt='back to top' />
		</TopStyle>
		<FooterStyle>{text}</FooterStyle>
	</div>
);

export default Footer;
