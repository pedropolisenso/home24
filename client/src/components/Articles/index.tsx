import React from 'react';
import { State } from '../../types/types';
import ArticleCount from './ArticleCount';
import ArticlesList from './ArticleList';

import { ContentStyle } from './styles/ArticlesStyles';

const MainContent = ({ state }: { state: State }) => (
	<ContentStyle>
		<ArticleCount state={state} />
		<ArticlesList categories={state.categories} />		
	</ContentStyle>
);

export default MainContent;
