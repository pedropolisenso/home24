import React from 'react';
import { Category } from '../../types/types'
import ArticleItem from './ArticleItem';

import { ArticleListStyle } from './styles/ArticlesStyles';

const ArticleList = ({ categories }: { categories: Category[] }) => {
	return (
		<ArticleListStyle>
			{categories?.map((category: Category, key: number) => (
				<ArticleItem key={key} category={category} />
			))}
		</ArticleListStyle>
	);
};

export default ArticleList;
