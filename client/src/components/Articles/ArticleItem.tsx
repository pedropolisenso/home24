import React from 'react';
import { Article, Category, CategoryArticle } from '../../types/types';
import { formatter } from '../../helpers/currency';

import { ArticlesGroupStyle, ArticlesItemStyle } from './styles/ArticlesStyles';

const ArticleItem = ({ category }: { category: Category }) => {
  const { articles }: CategoryArticle = category.categoryArticles;

	return (
		<ArticlesGroupStyle>
			{
				articles.map((article: Article, key: number) => {
					return (
						<ArticlesItemStyle key={key}>
							<img src={article.images[0].path} alt={article.name} />
							<div className='article-name'>{article.name}</div>
							<div>{formatter.format(article.prices.regular.value / 100)}</div>
							<button type="button" role="button" className="button">Add to Cart</button>
						</ArticlesItemStyle>
					);
				})
			}
		</ArticlesGroupStyle>
	);
};

export default ArticleItem;
