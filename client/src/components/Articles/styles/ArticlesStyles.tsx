import styled from '@emotion/styled'
import { theme } from '../../../theme';

export const ContentStyle = styled.div`
  grid-area: content;
  margin-top: -15px;
`;

export const ArticleListStyle = styled.div``;

export const ArticlesGroupStyle = styled.div`
  display: grid;
  grid-gap: 26px;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
`;

export const ArticlesItemStyle = styled.div`
  border: 1px solid ${theme.content.product.border.color};
  padding: 0 10px;
  float: left; 
  min-height: 180px;
  margin: 2px; 
  min-width: 117px;
  position:relative;
  @media (max-width: 907px) {
    height: auto;
  }

  > * {
    display: inline-block;
    padding: 4px 0;
    margin: 4px 0;
    width: 100%;
  }

  > .article-name {
    min-height: 55px;
    @media (max-width: 907px) {
        min-height: 35px;
    }
  }

  > .button {
    background-color: ${theme.content.product.button.background};
    color: ${theme.textColor}
    cursor: pointer;
    text-align: center;
    bottom: 0;
    left: 0;
    padding: 12px 0;
    margin-bottom: 10px;
    border-radius: 3px;
    border: 1px solid ${theme.content.product.border.color};
    font-weight: bold;
    transition: 0.1s;
    outline: none;
    &:hover {
        border: 1px solid ${theme.textColor};
        cursor: pointer;
    }
  }
`;
