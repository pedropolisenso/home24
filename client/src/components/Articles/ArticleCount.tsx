import React from 'react';
import { State } from '../../types/types';

const ArticleCount = ({ state }: { state: State }) => (
	state.categories?.length ? (
		<p style={{
      marginTop: 70
    }}>
			{state.categories[0].name} > Alles
			<small> ({state.categories[0].articleCount})</small>
    </p>
  ) : (
    <span style={{ display: 'block', marginTop: 12 }}>
      Loading products...
    </span>
  )
);

export default ArticleCount;
