import React from 'react';
import { State } from "../../types/types";
import SidebarNavigation from './SidebarNavigation';

import { SidebarStyle } from './styles/SidebarStyle';

const Sidebar = ({ state, title }: { state: State, title: string }) => (
	<SidebarStyle>
		<h3>{title}</h3>
		{state.categories?.length ? (
			<SidebarNavigation state={state} />
		) : (
			<span style={{ paddingLeft: 0 }}>Loading...</span>
		)}
	</SidebarStyle>
);

export default Sidebar;
