import styled from '@emotion/styled'
import { theme } from '../../../theme';

export const SidebarStyle = styled.div`
	grid-area: sidebar;
	background-color: ${theme.secondary};
	margin-top: 80px;
  	max-width: 240px;
	float: left;
	margin-right: 20px;

	span {
		padding-left: 12px;
	}

	h3 {
    padding: 0px;
    margin: 0 0 15px 0;
	}
`
 export const SidebarNavigationStyle = styled.ul`
 	list-style-type: none;
	margin: 0;
	padding: 0;
 `

 export const SidebarNavigationItemStyle = styled.li`
	padding: 12px 12px 12px 5px;
	border-bottom: 1px solid ${theme.sidebar.list.border.color};
	background-color: ${theme.secondary};
	transition: 0.3s;
  cursor: pointer;
	&:hover {
		background-color: ${theme.sidebar.list.hover.background};
	}
	&:last-child {
		border-bottom: none;
	  }

	a {
		text-decoration: none;
		padding: 0;
		color: ${theme.textColor}
	}
`;
