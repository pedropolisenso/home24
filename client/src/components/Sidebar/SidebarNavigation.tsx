import React from 'react';
import { State, ChildCategory } from "../../types/types";

import { SidebarNavigationStyle, SidebarNavigationItemStyle } from './styles/SidebarStyle';

const SidebarNavigation = ({ state }: { state: State }) => {
	const categories: ChildCategory[] = state.categories[0].childrenCategories.list;

	return (
		<SidebarNavigationStyle>
			{categories.map((child: ChildCategory, key: number) => {				
				return (
					<SidebarNavigationItemStyle key={key}>
						<a href={`/${child.urlPath}`}>{child.name}</a>
					</SidebarNavigationItemStyle>
				);
			})}
		</SidebarNavigationStyle>
	)
};

export default SidebarNavigation;
