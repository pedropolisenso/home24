export const theme: any = {
	primary: '#f45334',
	secondary: '#f7f7f7',
	textColor: '#2c3638', 

	header: {
		background: '#FFF',
		boxShadow: {
			color: '#e2e4e4',
		},
		search: {
			button: {
				color: '#ffff',
			},
		},
	},

	sidebar:{
		list: {
			border: {
				color: '#e2e4e4',
			},
			hover: {
				background: '#c3c3c3',
			},
		},
	},

	content: {
		product: {
			border: {
				color: '#bec4c4'
			},
			button: {
				background: '#FFF',
			},
		},
	},

	footer: {
		background: '#c3c3c3',
		color: '#2c3638',
	},
};
