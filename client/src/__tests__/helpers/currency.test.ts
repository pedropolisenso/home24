import { formatter } from '../../helpers/currency';

describe('Helpers', () => {
	it('returns correct formmater for currencies ', () => {
        expect(formatter.format(4000 / 100)).toEqual('40,00 €');
        expect(formatter.format(40000 / 100)).toEqual('400,00 €');
    });
});
