import React from 'react';
import { shallow } from 'enzyme';

import Footer from '../../../components/Footer';
import { FooterStyle, TopStyle } from '../../../components/Footer/styles/FooterStyles';

describe('Footer', () => {
	let text: string;

	beforeAll(() => {
		text = 'copyright';
	});

	it('renders the Footer', () => {
		const component = shallow(<Footer text={text} />);

        expect(component.find(FooterStyle)).toHaveLength(1);
        expect(component.find(FooterStyle).props().children).toEqual('copyright');
        expect(component.find(TopStyle)).toHaveLength(1);
    });    
});
