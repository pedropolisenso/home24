import React from 'react';
import { shallow } from 'enzyme';

import ArticleCount from '../../../components/Articles/ArticleCount';

describe('ArticleCount', () => {
	let props: any;

	beforeAll(() => {
		props = {
            state: {
                categories: [{
                    name: 'Sofa',
                    articleCount: 12345,
                }],
            }
        };
	});

	it('renders the ArticleCount', () => {
		const component = shallow(<ArticleCount {...props} />);

        expect(component.find('small').props().children).toEqual([ ' (', 12345, ')' ]);
    });

    it('should renders loading component if there is not categories', () => {
        const customProps = {
            ...props,
            state: {
                categories: [],
            },
        };
		const component = shallow(<ArticleCount {...customProps} />);

        expect(component.find('span')).toHaveLength(1);
        expect(component.find('span').props().children).toEqual('Loading products...');
    });
});
