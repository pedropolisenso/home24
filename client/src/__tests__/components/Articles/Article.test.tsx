import React from 'react';
import { shallow } from 'enzyme';

import Article from '../../../components/Articles';

import ArticleCount from '../../../components/Articles/ArticleCount';
import ArticleList from '../../../components/Articles/ArticleList';

describe('Article', () => {
	let props: any;

	beforeAll(() => {
		props = {
            state: {
                categories: null,
            }
        };
	});

	it('renders the Article', () => {
		const component = shallow(<Article {...props} />);

        expect(component.find(ArticleCount)).toHaveLength(1);
        expect(component.find(ArticleList)).toHaveLength(1);
    });    
});
