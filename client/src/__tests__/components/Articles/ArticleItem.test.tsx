import React from 'react';
import { shallow } from 'enzyme';

import ArticleItem from '../../../components/Articles/ArticleItem';
import { ArticlesItemStyle } from '../../../components/Articles/styles/ArticlesStyles';

describe('ArticleItem', () => {
	let props: any;

	beforeAll(() => {
		props = {
            category: {
                categoryArticles: {
                    articles: [
                        {
                            name:  'sofa',
                            variantName: 'sofa test',
                            prices: {
                                currency: 'EUR',
                                regular: {
                                    value: 4000,
                                }
                            },
                            images: [{
                                path: 'path',
                            }],
                        },
                        {
                            name:  'table',
                            variantName: 'table test',
                            prices: {
                                currency: 'EUR',
                                regular: {
                                    value: 2000,
                                }
                            },
                            images: [{
                                path: 'path',
                            }],
                        }
                    ],
                },                
            },
        };
	});

	it('renders the ArticleItem', () => {
        const component = shallow(<ArticleItem {...props} />);
        
        expect(component.find(ArticlesItemStyle)).toHaveLength(2);
    });

    it('should renders 4 elements per Item', () => {
        const component = shallow(<ArticleItem {...props} />);
        expect(component.find(ArticlesItemStyle).at(0).props().children).toHaveLength(4);
    });
});
