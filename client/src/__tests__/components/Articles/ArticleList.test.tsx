import React from 'react';
import { shallow } from 'enzyme';

import ArticleList from '../../../components/Articles/ArticleList';
import ArticleItem from '../../../components/Articles/ArticleItem';

describe('ArticleList', () => {
	let props: any;

	beforeAll(() => {
		props = {
            categories: [{
                name: 'test',
                categoryArticles: {
                    articles: [{
                        name:  'sofa',
                        variantName: 'sofa test',
                        prices: '4000',
                        images: [{
                            path: 'path',
                        }],
                    }],
                },
            }],
        };
	});

	it('renders the ArticleList', () => {
        const component = shallow(<ArticleList {...props} />);
        
        expect(component.find(ArticleItem)).toHaveLength(1);
    });
});
