import React from 'react';
import { shallow } from 'enzyme';

import Sidebar from '../../../components/Sidebar';
import SidebarNavigation from '../../../components/Sidebar/SidebarNavigation';

describe('Sidebar', () => {
	let props: any;

	beforeAll(() => {
		props = {
            title: 'categories',
            state: {
                categories: [{
                    childrenCategories: [
                        {
                            urlPath: 'http://test1.conm',
                            name: 'category 1',
                        },
                        {
                            urlPath: 'http://test2.conm',
                            name: 'category 2',
                        },
                        {
                            urlPath: 'http://test3.conm',
                            name: 'category 3',
                        },
                    ],
                }],
            },
        };
	});

	it('renders the Sidebar', () => {
		const component = shallow(<Sidebar {...props} />);
		
        expect(component.find('h3')).toHaveLength(1);
        expect(component.find('h3').props().children).toEqual('categories');	
    });
    
    it('should renders navigation componet if there is the list', () => {
        const component = shallow(<Sidebar {...props} />);
		
        expect(component.find(SidebarNavigation)).toHaveLength(1);
        expect(component.find('span')).toHaveLength(0);
    });

    it('should render loading text if there is not the list', () => {
        const customProps = {
            ...props,
            state: {
                categories: [],
            },
        };

        const component = shallow(<Sidebar {...customProps} />);
		
        expect(component.find(SidebarNavigation)).toHaveLength(0);
        expect(component.find('span')).toHaveLength(1);
    });

});
