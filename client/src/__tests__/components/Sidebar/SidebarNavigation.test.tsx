import React from 'react';
import { shallow } from 'enzyme';

import SidebarNavigation from '../../../components/Sidebar/SidebarNavigation';
import {
    SidebarNavigationStyle as ul,
    SidebarNavigationItemStyle as li
} from '../../../components/Sidebar/styles/SidebarStyle';

describe('SidebarNavigation', () => {
	let props: any;

	beforeAll(() => {
		props = {
            state: {
                categories: [{
                    childrenCategories: {
                        list: [
                            {
                                urlPath: 'http://test1.conm',
                                name: 'category 1',
                            },
                            {
                                urlPath: 'http://test2.conm',
                                name: 'category 2',
                            },
                            {
                                urlPath: 'http://test3.conm',
                                name: 'category 3',
                            },
                        ],
                    }
                }],
            },
        };
	});

	it('renders the SidebarNavigation with items', () => {
		const component = shallow(<SidebarNavigation {...props} />);

        expect(component.find(ul)).toHaveLength(1);
        expect(component.find(li)).toHaveLength(3);
        expect(component.find(li).at(0).text()).toEqual('category 1');
        expect(component.find(li).at(1).text()).toEqual('category 2');
        expect(component.find(li).at(2).text()).toEqual('category 3');
    });    
});
