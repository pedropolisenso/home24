import React from 'react';
import { shallow } from 'enzyme';

import Header from '../../../components/Header';

describe('Header', () => {
	let title: string;

	beforeAll(() => {
		title = 'Home24';
	});

	it('renders the Header', () => {
		const component = shallow(<Header title={title} />);
		
		expect(component.find('img')).toHaveLength(1);
		expect(component.find('img').props().alt).toEqual(title);
		expect(component.find('input')).toHaveLength(1);
	});

	it('renders the Header with other title', () => {
		let title = 'other tilte';
		const component = shallow(<Header title={title} />);
		
		expect(component.find('img').props().alt).toEqual('other tilte');
	});
});
