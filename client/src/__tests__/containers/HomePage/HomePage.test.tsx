import React from 'react';
import { shallow } from 'enzyme';

import HomePage from '../../../containers/HomePage';

import Header from '../../../components/Header';
import Sidebar from '../../../components/Sidebar';
import MainContent from '../../../components/Articles';
import Footer from '../../../components/Footer';

describe('HomePage', () => {
	it('renders the HomePage', () => {
		const component = shallow(<HomePage />);

        expect(component.find(Header)).toHaveLength(1);
        expect(component.find(Sidebar)).toHaveLength(1);
        expect(component.find(MainContent)).toHaveLength(1);
        expect(component.find(Footer)).toHaveLength(1);
    });
});
