import React from 'react';
import { render } from 'enzyme';

import Services from '../../services/fetch';
import { HomePage } from '../../containers/HomePage';

describe('Services class', () => {
  const services = new Services()
  const setStateMock = () => {
    return {
      categories: [{
        childrenCategories: {
          list: [{
            name: 'sofa',
            urlPath: 'https://url/path/sofa.jpg'
          }]
        }
      }]
    }
  };

  const useStateMock: any = (useState: any) => [useState, setStateMock];

  jest.spyOn(React, "useState").mockImplementation(useStateMock);
  jest.spyOn(React, "useEffect").mockImplementation((f: any) => f());
  jest.spyOn(services, 'POST')

  it('WHEN there are list to render THEN the API is not called', () => {  
    render(<HomePage />);
    expect(services.POST).toHaveBeenCalledTimes(0)
  });
});
