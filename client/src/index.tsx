import React from 'react';
import ReactDOM from 'react-dom';
import HomePage from './containers/HomePage';

import './styles/index.css';

ReactDOM.render(
  <React.StrictMode>
    <HomePage />
  </React.StrictMode>,
  document.getElementById('root')
);
