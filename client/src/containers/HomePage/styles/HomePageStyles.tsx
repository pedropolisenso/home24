import styled from '@emotion/styled'

export const HomePageStyle = styled.div`
    grid-template-areas:
        'header header header'
        'sidebar content'
        'footer footer footer';

    > * {
        padding: 10px;
    }
`