import React, { useEffect, useState } from 'react';
import Header from '../../components/Header';
import Sidebar from '../../components/Sidebar';
import MainContent from '../../components/Articles';
import Footer from '../../components/Footer';
import { State } from '../../types/types';
import { fetchAPI } from '../../services/fetch';

import { HomePageStyle } from './styles/HomePageStyles';

export const HomePage = () => {
  const [state, setState] = useState<State>({categories: []});

  useEffect(() => {
    if (!state.categories?.length) {
      fetchAPI.POST('/graphql').then((data: State) => {
        setState({ categories: data?.categories });
      });
    }
  });

  return (
    <HomePageStyle>
      <Header title='home24' />
      <Sidebar title='Kategorien' state={state} />
      <MainContent state={state} />
      <Footer text='Alle Preise sind in Euro (€) inkl. gesetzlicher Umsatzsteuer und Versandkosten.' />
    </HomePageStyle>
  );
};

export default HomePage;
