## Things I would like to improve or to do
- Maybe pagination
- Maybe a centralized state (Redux/ContextAPI) it works better than state component and avoid promise inside components

## Info about app
- This is simple app and I chose "styled component" for css/style, because the current version it looks good for performance and a bit easy for understand.

- I'm using Typescript for every variables, params, props and etc, because I wanna make sure the data will be correct for components or functions.

- I thought a bit about accessibility as well, so I added important properties to images, buttons and links.

- I created Unit Tests for app, because I believe I can make sure the functionality with tests.
